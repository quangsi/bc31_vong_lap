// for( khởi tạo ; điều kiện ; bước nhảy)
// {
//     hành động
// }

// khởi tạo : chỉ dc gọi 1 lần duy nhất trong suốt quá trình lặp

// bước nhảy sẽ tự động được gọi sau khi hành động kết thúc

for (var index = 0; index < 5; index++) {
  // %2  0 1
  console.log(index);
}

//  dự đoán biến count

var count = 0;

for (count; count <= 10; count++) {
  console.log("Do nothing");
}

console.log(count);

//  bài 1
function sayHello() {
  console.log("hello");
}
// anonymous function
document.getElementById("btn-bai-1").addEventListener("click", function () {
  var contentSoChan = "";
  var contentSoLe = "";
  for (var i = 0; i <= 100; i++) {
    if (i % 2 == 0) {
      contentSoChan = contentSoChan + "  " + i;
    } else {
      contentSoLe = contentSoLe + "  " + i;
    }
  }
  document.getElementById(
    "ket-qua-bai-1"
  ).innerHTML = ` ${contentSoChan} <br/>  ${contentSoLe}`;
});

// bài 2

// 9 => 2+4+6+8 = 20
document.getElementById("btn-bai-2").addEventListener("click", function () {
  var numberValue = document.getElementById("input-bai-2").value * 1;
  var sum = 0;
  for (var i = 1; i <= numberValue; i++) {
    if (i % 2 == 0) {
      sum = sum + i;
    }
  }
});
