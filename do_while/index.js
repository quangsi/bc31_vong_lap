var number = 10;
while (number > 20) {
  console.log("yes");
}

// do{
//     hành động
// }while( điều kiện)

// => làm trước kiểm tra sau

do {
  console.log("hello", number);
  number--;
} while (number > 0);

// in => 1

// ko in =>0

function inKetQua() {
  console.log("yes");
  var numberValue = document.getElementById("txt-number").value * 1;

  console.log({ numberValue });
  var sum = 0;
  var count = 1;

  var contentHTML = "";
  do {
    sum = sum + count;
    var content = `<div> count: ${count} - sum: ${sum} <div>`;
    contentHTML += content; //contentHTML = contentHTML + content

    count++;

    console.log({ sum, count });
  } while (count <= numberValue);

  document.getElementById("ket-qua").innerHTML = contentHTML;
}
// 3 => 1+2+3
